numOfLines = int(input())
totalVotes = {}
for line in range(numOfLines):
    k, v = input().split()
    if str(k) not in totalVotes.keys():
        totalVotes[str(k)] = int(v)
    else:
        totalVotes[str(k)] += int(v)
for key, val in sorted(totalVotes.items()):
    print(key, val)