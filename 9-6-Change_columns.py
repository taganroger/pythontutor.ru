def swap_columns(a, i, j):
    for row in range(len(a)):
        a[row][i], a[row][j] = a[row][j], a[row][i]
    return a

n, m = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(n)]
i, j = [int(s) for s in input().split()]

for row in swap_columns(a, i, j):
    print(' '.join([str(elem) for elem in row]))