N,K=[int(i) for i in input().split()]
weekends=set(range(7, N + 1, 7))
weekends.update(set(range(6, N + 1, 7)))
allStrikeSet=set()
for i in range(K):
    a_i, b_i =[int(j) for j in input().split()]
    allStrikeSet.update(set(range(a_i, N + 1, b_i)))
allStrikeSet -= weekends
print(len(allStrikeSet))