import math
degrees_ = float(input())

hours_ = degrees_ // 30

#В 360 градусах 720 минут - в одном градусе 2 минуты
#В 360 градусах 43200 секунд - в одном градусе 120 секунд

minutes_ = math.floor( (degrees_ - hours_ * 30) * 2)
minutes_4 = math.floor( degrees_ % 30 * 2)
minutes_3 = (degrees_ - hours_ * 30) * 2
# better is
minutes_2 = degrees_ % 30 * 2
seconds_ = (degrees_ - hours_ * 30 - minutes_ * 0.5) * 120
print(minutes_2, minutes_, minutes_3, minutes_4)
print(int(hours_), int(minutes_), int(seconds_))

#print(int(degrees_ // 30), int(degrees_ % 30 * 2), int(degrees_ % 0.5 * 120))