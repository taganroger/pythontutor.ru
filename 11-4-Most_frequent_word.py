numOfLines = int(input())
quantityDict = {}
for line in range(numOfLines):
    for word in input().split():
        quantityDict[word] = quantityDict.get(word, 0) + 1
for k, v in sorted(quantityDict.items()):
    if v == sorted(quantityDict.items(), key = lambda x: x[1], reverse = True)[0][1]:
        print(k)
        break