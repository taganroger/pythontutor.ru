potentialNumbers = set(range(1, int(input()) + 1))
while True:
    numbers = input()
    if numbers == 'HELP':
        break
    tempSet = potentialNumbers & set(int(i) for i in numbers.split())
    if len(tempSet) <= len(potentialNumbers) // 2:
        print('NO')
        potentialNumbers -= tempSet
    else:
        print('YES')
        potentialNumbers &= tempSet
print(*sorted(potentialNumbers))