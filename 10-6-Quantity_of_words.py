rowsNum = int(input())
words = set()
for row in range(rowsNum):
    words.update(input().split())
print(len(words))