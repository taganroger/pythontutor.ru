initialList = [int(s) for s in input().split()]
height2Check = int(input())
for i in range(len(initialList) - 1, -1, -1):
    if initialList[i] >= height2Check:
        print(i + 2)
        break
else:
    print(1)