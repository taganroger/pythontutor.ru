quantityOfQueens = 8
queensPos =[]
resultValue = False
for queensNum in range(quantityOfQueens):
    queensPos.append([int(s) for s in input().split()])
for externalElem in queensPos:
    for internalElem in queensPos:
        if externalElem != internalElem:
            if (externalElem[0] == internalElem[0] or externalElem[1] == internalElem[1]) or \
                    (abs( externalElem[0] - internalElem[0] ) == abs(externalElem[1] - internalElem[1])):
                resultValue = True
                break
    if resultValue:
        break
if resultValue:
    print('YES')
else:
    print('NO')