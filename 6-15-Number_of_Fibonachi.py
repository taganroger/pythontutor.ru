A = int(input())
tempCounter1 = 0
tempCounter2 = 1
mainCounter = 0
while True:
    if tempCounter1 == A:
        break
    elif tempCounter1 > A:
        mainCounter = -1
        break
    tempCounter1, tempCounter2 = tempCounter2, tempCounter1 + tempCounter2
    mainCounter += 1
print(mainCounter)