sourceNumber = int(input())
if sourceNumber < 100 or sourceNumber > 999:
    print('Number is not valid!')
else:
    thirdDigit = sourceNumber % 10
    secondDigit = sourceNumber % 100 // 10
    firstDigit = sourceNumber // 100
    print(firstDigit + secondDigit + thirdDigit)