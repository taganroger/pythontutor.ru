n = int(input())
matrix = [[0] * n for i in range(n)]
for row in range(n):
    for column in range(n):
        matrix[row][column] = abs(row - column)
for row in matrix:
    print(' '.join([str(elem) for elem in row]))