currentNum = int(input())
maxNum = currentNum
counter_ = 1
newMaxFlag = False
while currentNum != 0:
    currentNum = int(input())
    if maxNum == currentNum:
        if newMaxFlag:
            newMaxFlag = False
            counter_ = 1
        counter_ += 1
    elif maxNum < currentNum:
        maxNum = currentNum
        newMaxFlag = True
print(counter_)