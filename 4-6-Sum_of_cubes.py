n = int(input())
final_sum = 0
for i in range(1, n + 1):
    final_sum += i ** 3
print(final_sum)