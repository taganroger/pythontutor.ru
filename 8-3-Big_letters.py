def capitalize(initialString):
    finalList = []
    for words in initialString:
        temp_word = chr(ord(words[0]) - 32) + words[1:]
        finalList.append(temp_word)
        finalString = ' '.join(finalList)
    return finalString

print(capitalize(input().split()))