def isRelated(person1, person2, dict):
    person2Check = person1
    while person2Check in dict:
        person2Check = dict[person2Check]
        if person2Check == person2:
            return 2
    person2Check = person2
    while person2Check in dict:
        person2Check = dict[person2Check]
        if person2Check == person1:
            return 1
    return 0

numOfPeople = int(input())
descDict = {}
for person in range(numOfPeople - 1):
    curPerson, ancestor = input().split()
    descDict[curPerson] = ancestor
tasks = int(input())
for task in range(tasks):
    person1, person2 = input().split()
    print(isRelated(person1, person2, descDict))

'''
Main table
descendant ancestor

Second table
If first ancestor - 1
If first descendant - 2
If not related - 0
'''