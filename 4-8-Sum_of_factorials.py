n = int(input())
final_sum = 0
factorial_multiplier = 1
for i in range(1, n + 1):
    factorial_multiplier *= i
    final_sum += factorial_multiplier
print(final_sum)