studLangList = []
studentsQuantity = int(input())
allLangs = set()
for student in range(studentsQuantity):
    studLangList.append([])
    langQuantity = int(input())
    for language in range(langQuantity):
        studLangList[student].append(str(input()))
        allLangs.add(studLangList[student][language])
commonLangs = set(studLangList[0])
for student in range(1, studentsQuantity):
    commonLangs &=  set(studLangList[student])
print(len(commonLangs))
for i in sorted(commonLangs):
    print(i)
print(len(allLangs))
for j in sorted(allLangs):
    print(j)