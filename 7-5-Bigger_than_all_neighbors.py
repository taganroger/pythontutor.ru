initialList = [int(s) for s in input().split()]
counter_ = 0
for i in range(1, len(initialList) - 1):
    if initialList[i] > initialList[i - 1] and initialList[i] > initialList[i + 1]:
        counter_ += 1
print(counter_)