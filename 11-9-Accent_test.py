#Import dictionary as a set.
words = int(input())
dict = set()
for word in range(words):
    dict.add(str(input()))
#Import text as a list and create additional set from it.
textFull = [str(s) for s in input().split()]
textCheck = set(textFull)
#Subtract dictionary set from text set.
textCheck -= dict
#Subtract words with one uppercase letters from text set.
originalTextCheck = textCheck.copy()
upperTextCheck = {x.upper() for x in dict}
for word in originalTextCheck:
    upperCounter = 0
    for letter in word:
        if letter.isupper():
            upperCounter += 1
            if upperCounter == 2:
                break
    if upperCounter == 1 and word.upper() not in upperTextCheck:
        textCheck.remove(word)

# Comparing set of errors with initial list
errorCounter = 0
for word in textFull:
    if word in textCheck:
        errorCounter += 1
print(errorCounter)