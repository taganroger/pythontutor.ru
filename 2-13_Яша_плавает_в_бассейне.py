N=int(input())
M=int(input())
x=int(input())
y=int(input())

if M > N:
    N, M = M, N
# Сейчас длинная сторона - N, а короткая - M
# Определяем куда короче плыть, в определенную сторону или в противоположную от нее.
if M - x < x:
    x = M - x
if N - y < y:
    y = N - y
# А теперь выбираем короткую из оставших двух
if x < y:
    print(x)
else:
    print(y)