def findAncestors(descendant, dict):
    ancList = [descendant]
    while descendant in dict:
        descendant = dict[descendant]
        ancList.append(descendant)
    return ancList

numOfPeople = int(input())
mainDict = {}
for person in range(numOfPeople - 1):
    curPerson, ancestor = input().split()
    mainDict[curPerson] = ancestor
tasks = int(input())
for task in range(tasks):
    person1, person2 = input().split()
    ancestorsFirst = set(findAncestors(person1, mainDict))
    for ancestor in findAncestors(person2, mainDict):
        if ancestor in ancestorsFirst:
            print(ancestor)
            break