def printCubes(superSet):
    print(len(superSet))
    print(*sorted(superSet))

N, M = input().split()
AnnCubesSet = set()
for i in range(int(N)):
    AnnCubesSet.add(int(input()))
BorisCubesSet = set()
for j in range(int(M)):
    BorisCubesSet.add(int(input()))
printCubes(AnnCubesSet & BorisCubesSet)
printCubes(AnnCubesSet - BorisCubesSet)
printCubes(BorisCubesSet - AnnCubesSet)