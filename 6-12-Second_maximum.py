currentNum = int(input())
maxFirst = 0
maxSecond = 0
while currentNum != 0:
    if maxFirst < currentNum:
        maxSecond = maxFirst
        maxFirst = currentNum
    elif maxSecond < currentNum < maxFirst:
        maxSecond = currentNum
    currentNum = int(input())
print(maxSecond)