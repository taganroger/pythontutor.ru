def power(base, degree):
    result = base
    if degree == 0:
        return 1
    elif degree == 1:
        return result
    elif degree == -1:
        return 1 / result
    elif degree > 1:
        for temp in range(2, degree + 1):
            result *= base
        return result
    elif degree < -1:
        for temp in range(2, - degree + 1):
            result *= base
        return 1 / result
    return result

a = float(input())
n = int(input())
print(power(a, n))