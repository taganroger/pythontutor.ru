N = int(input())
exponentResult = 1
exponent_ = 0
while exponentResult <= N:
    if exponentResult * 2 > N:
        break
    exponentResult *= 2
    exponent_ += 1
print(exponent_, exponentResult)