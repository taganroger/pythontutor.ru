initialList = [int(s) for s in input().split()]
k, c = [int(s) for s in input().split()]
initialList.insert(k, c)
print(' '.join([str(out_) for out_ in initialList]))