n = int(input())
matrix = [[0] * n for i in range(n)]
for row in range(n):
    matrix[row][n - 1 - row] = 1
    for col in range(n - row, n):
        matrix[row][col] = 2
for row in matrix:
    print(' '.join([str(elem) for elem in row]))