numOfLines = int(input())
quantityDict = {}
for line in range(numOfLines):
    for word in input().split():
        quantityDict[word] = quantityDict.get(word, 0) + 1
resultList = []
for word, num in quantityDict.items():
    resultList.append((1000 - num, word))
for sortPair in sorted(resultList):
    print(sortPair[1])