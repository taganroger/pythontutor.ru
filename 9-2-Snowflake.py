n = int(input())
snowflakeList = [['.'] * n for i in range(n)]
for row in range(n):
    for column in range(n):
        if column == n // 2 or row == n // 2 or row == column or row + column == n - 1 :
            snowflakeList[row][column] = '*'
for row in snowflakeList:
    print(' '.join([str(elem) for elem in row]))
    #print(' '.join(row))