currentNum = int(input())
prevNum = 0
interSeq = 1
finalSeq = 0
while currentNum != 0:
    if currentNum == prevNum:
        interSeq += 1
    else:
        if interSeq > finalSeq:
            finalSeq = interSeq
        interSeq = 1
    prevNum = currentNum
    currentNum = int(input())
if interSeq > finalSeq:
    finalSeq = interSeq
print(finalSeq)