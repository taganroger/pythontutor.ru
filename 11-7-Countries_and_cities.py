numOfCountries = int(input())
countriesDict = {}
for file in range(numOfCountries):
    country2add = [str(i) for i in input().split()]
    countriesDict[country2add[0]] = country2add[1:len(country2add)]
numOfCities = int(input())
for city in range(numOfCities):
    cityName = str(input())
    for k, v in countriesDict.items():
        if cityName in v:
            print(k)