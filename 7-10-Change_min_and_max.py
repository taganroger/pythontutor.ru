initialList = [int(s) for s in input().split()]
minIndex = initialList.index(min(initialList))
maxIndex = initialList.index(max(initialList))
initialList[minIndex], initialList[maxIndex] = initialList[maxIndex], initialList[minIndex]
print(' '.join([str(out_) for out_ in initialList]))