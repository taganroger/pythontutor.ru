lessons = int(input())
evenNum = (lessons - 1) // 2
unevenNum = lessons - 1 - evenNum

totalTimeMin = lessons * 45 + unevenNum * 5 + evenNum * 15
print((totalTimeMin // 60) + 9, totalTimeMin % 60)
