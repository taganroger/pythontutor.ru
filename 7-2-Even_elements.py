A = [int(s) for s in input().split()]
for elem in A:
    if elem % 2 == 0:
        print(elem, end=' ')