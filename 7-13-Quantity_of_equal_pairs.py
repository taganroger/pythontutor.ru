initialList = [int(s) for s in input().split()]
equCounter = 0
for i in range(len(initialList)):
    equCounter += initialList[i + 1::].count(initialList[i])
print(equCounter)