potentialNumbers = set(range(1, int(input()) + 1))
while True:
    numbers = input()
    if numbers == 'HELP':
        break
    answer = input()
    if answer == 'YES':
        potentialNumbers &= set(int(i) for i in numbers.split())
    else:
        potentialNumbers -= set(int(i) for i in numbers.split())
print(*potentialNumbers)