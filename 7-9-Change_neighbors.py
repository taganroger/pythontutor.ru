initialList = [int(s) for s in input().split()]
for i in range(0, len(initialList), 2):
    if i + 1 == len(initialList):
        break
    initialList[i], initialList[i + 1] = initialList[i + 1], initialList[i]
for num2Print in initialList:
    print(num2Print, end=' ')

#print(' '.join([str(out_) for out_ in initialList]))