numOfWords = int(input())
engLatDict = {}
for word in range(numOfWords):
    engWord2add = [str(i) for i in input().replace('-', ' ').replace(',', ' ').split()]
    engLatDict[engWord2add[0]] = engWord2add[1:len(engWord2add)]
latEngDict = {}
for latListWord in engLatDict.values():
    for latWord in latListWord:
        latEngDict[latWord] = []

for engWord, v in engLatDict.items():
    for latWord in v:
        latEngDict[latWord].append(engWord)

print(len(latEngDict))
for latWord, engWord in sorted(latEngDict.items()):
    print(latWord, end='')
    print(' - ', end='')
    for engListword in sorted(engWord):
        if engListword == engWord[-1]:
            print(engListword)
        else:
            print(engListword, end='')
            print(', ', end='')

'''from collections import defaultdict

latin_to_english = defaultdict(list)
for i in range(int(input())):
    english_word, latin_translations_chunk = input().split(' - ')
    latin_translations = latin_translations_chunk.split(', ')
    for latin_word in latin_translations:
        latin_to_english[latin_word].append(english_word)
    
print(len(latin_to_english))
for latin_word, english_translations in sorted(latin_to_english.items()):
    print(latin_word + ' - ' + ', '.join(english_translations))'''