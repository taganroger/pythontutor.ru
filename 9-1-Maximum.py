n, m = [int(s) for s in input().split()]
initialList = [[int(j) for j in input().split()] for i in range(n)]
var2Check = initialList[0][0]
maxRow = 0
maxCol = 0
for row in range(n):
    for column in range(m):
        if initialList[row][column] > var2Check:
            var2Check = initialList[row][column]
            maxRow = row
            maxCol = column
print(maxRow, maxCol)
