N, K = [int(s) for s in input().split()]
bowlingList = ['I'] * N
for tries in range(K):
    startPoint, endPoint = [int(s) for s in input().split()]
    bowlingList[startPoint - 1 : endPoint ] = '.' * (endPoint - startPoint + 1)
for elem in bowlingList:
    print(elem, end='')