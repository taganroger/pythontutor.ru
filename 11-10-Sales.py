stringSales = [s for s in input().split()]
salesDict = {}
while True:
    try:
        if stringSales[0] not in salesDict:
            good = {}
            good[stringSales[1]] = stringSales[2]
            salesDict[stringSales[0]] = good.copy()
        else:
            if stringSales[1] not in salesDict[stringSales[0]]:
                salesDict[stringSales[0]][stringSales[1]] = int(stringSales[2])
            else:
                salesDict[stringSales[0]][stringSales[1]] = int(salesDict[stringSales[0]][stringSales[1]]) + int(stringSales[2])
        stringSales = [s for s in input().split()]
        '''
        f, p, c = input().split()
        d[f] = d.get(f, {})
        d[f][p] = d[f].get(p, 0) + int(c)
        '''
    except:
        break

#https://code-maven.com/how-to-insert-a-dictionary-in-another-dictionary-in-python

for name in sorted(salesDict.keys()):
    print(name + ':')
    for good in sorted(salesDict[name].keys()):
        print(good + ' ' + str(salesDict[name][good]))