initialList = [int(s) for s in input().split()]
tempVariable = initialList[0]
numCounter = 1
for i in range(1, len(initialList)):
    if initialList[i] != tempVariable:
        numCounter += 1
        tempVariable = initialList[i]
print(numCounter)