initialList = [int(s) for s in input().split()]
compareSet = set()
for num in initialList:
    if num not in compareSet:
        print('NO')
        compareSet.add(num)
    else:
        print('YES')
