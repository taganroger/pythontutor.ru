n, m = [int(s) for s in input().split()]
initialDesk = [['.'] * m for i in range(n)]
for row in range(n):
    for column in range((row + 1) % 2 , m, 2):
        initialDesk[row][column] = '*'
for row in initialDesk:
    print(' '.join(row))