numOfFiles = int(input())
filesDict = {}
for file in range(numOfFiles):
    file2add = [str(i) for i in input().split()]
    filesDict[file2add[0]] = file2add[1:len(file2add)]
numOfOperations = int(input())
for operation in range(numOfOperations):
    operation2Execute = [str(i) for i in input().split()]
    operation2Execute[0] = operation2Execute[0][0].upper()
    if operation2Execute[0] == 'E':
        operation2Execute[0] = 'X'
    if operation2Execute[0] in filesDict[operation2Execute[1]]:
        print('OK')
    else:
        print('Access denied')

'''ACTION_PERMISSION = {
    'read': 'R',
    'write': 'W',
    'execute': 'X',
}

file_permissions = {}
for i in range(int(input())):
    file, *permissions = input().split()
    file_permissions[file] = set(permissions)

for i in range(int(input())):
    action, file = input().split()
    if ACTION_PERMISSION[action] in file_permissions[file]:
        print('OK')
    else:
        print('Access denied')
'''