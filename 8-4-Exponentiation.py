def power(base, degree):
    if degree == 0:
        return 1
    else:
        return base * power(base, degree - 1)

a = float(input())
n = int(input())
print(power(a, n))