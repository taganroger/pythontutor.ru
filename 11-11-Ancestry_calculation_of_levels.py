def levels(person, straightDict):
    if straightDict.get(person) == 0:
        return 0
    else:
        return levels(straightDict[person], straightDict) + 1

numOfPeople = int(input())
straightDict = {}

for person in range(numOfPeople - 1):
    curPerson, ancestor = input().split()
    straightDict[curPerson] = ancestor
    straightDict[ancestor] = straightDict.get(ancestor, 0)

for resultPerson in sorted(straightDict.keys()):
    print(resultPerson, levels(resultPerson, straightDict))